﻿using System;

namespace ClassDemo
{
    //Change for merge
     public class Calculator
    {
        //class members
        int num1, num2;
        int result;
        void Add()
        {
            result = num1 + num2;
            Console.WriteLine(result);
            Console.ReadLine();
        }
        void Sub()
        {
            result = num1 - num2;
            Console.WriteLine(result);
            Console.ReadLine();
        }


        static void Main(string[] args)
        {
                Console.WriteLine("Calculator: ");
                Calculator obj = new Calculator();
                obj.num1 = 10;
                obj.num2 = 20;
                obj.Add();
                obj.Sub();
            
        }
    }

}
